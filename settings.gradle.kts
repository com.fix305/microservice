pluginManagement {
    plugins {
        id("org.jetbrains.kotlin.jvm") version "1.9.0"
    }
}

rootProject.name = "com.fix305.microservice"
include("ktor", "example")
