package com.fix305.microservice.example.route

import com.fix305.microservice.example.IProductController
import com.fix305.microservice.ktor.KtorRoute
import com.fix305.microservice.ktor.respondResponse
import io.ktor.server.application.*
import io.ktor.server.routing.*

class ProductRoute(
    private val controller: IProductController
) : KtorRoute {
    override fun install(routing: Routing) {
        routing {
            route(ROOT_SEGMENT) {
                get {
                    call.respondResponse(controller.get())
                }

                get("{id}") {

                }

                post { }

                put("{id}") {

                }

                patch("{id}") { }
                delete("{id}") { }
            }
        }
    }

    companion object {
        private const val ROOT_SEGMENT = "products"
    }
}