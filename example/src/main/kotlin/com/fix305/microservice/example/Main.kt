package com.fix305.microservice.example

import com.fix305.microservice.example.route.ProductRoute
import com.fix305.microservice.ktor.KtorRoute
import com.fix305.microservice.ktor.microservice
import io.ktor.server.application.*
import io.ktor.server.netty.*
import org.kodein.di.bindSingleton
import org.kodein.di.inSet
import org.kodein.di.instance
import org.kodein.di.singleton

fun main(args: Array<String>) = EngineMain.main(args)

fun Application.module() = this.microservice {
    bindSingleton<IProductController> { ProductController(config = instance()) }
    inSet<KtorRoute> { singleton { ProductRoute(controller = instance()) } }
}