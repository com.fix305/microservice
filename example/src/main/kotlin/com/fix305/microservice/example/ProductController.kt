package com.fix305.microservice.example

import com.fix305.common.config.Config
import com.fix305.common.response.Response
import kotlinx.serialization.Serializable

interface IProductController {
    suspend fun get(): Response<ProductsDto>
}

@Serializable
data class ProductsDto(
    val products: List<ProductDto>
)

@Serializable
data class ProductDto(
    val id: Long,
)

class ProductController(private val config: Config) : IProductController {
    init {
        println("TEST ENV: ${config.getOrNull("TEST")?.getString()}")
        println("TEST FILE: ${config.getOrNull("some.value")?.getString()}")
    }

    /** Data from some product service converted to dto */
    private val products = mutableListOf<ProductDto>(
        ProductDto(
            id = 6636
        )
    )

    override suspend fun get(): Response<ProductsDto> {
        val result = ProductsDto(
            products = products
        )

        return Response.success(result)
    }
}