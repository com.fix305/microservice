plugins {
    id("org.jetbrains.kotlin.jvm") version "1.9.0"
    kotlin("plugin.serialization") version "1.9.0"
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/50393262/packages/maven")
}

val fix305CommonVersion: String by project

dependencies {
    implementation(project(":ktor"))

    implementation("com.fix305.common:config:$fix305CommonVersion")
    implementation("com.fix305.common:response:$fix305CommonVersion")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}