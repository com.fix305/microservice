package com.fix305.microservice.ktor

import com.fix305.common.config.Config
import com.fix305.common.config.ConfigProvider
import com.fix305.common.config.plus
import com.fix305.common.response.NoContent
import com.fix305.common.response.Response
import com.fix305.microservice.ktor.module.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.util.*
import org.kodein.di.*

private val diAttributeKey = AttributeKey<DI>("di")
val Application.di get() = this.attributes[diAttributeKey]

fun Application.microservice(builder: DI.Builder.() -> Unit) {
    val application = this

    val di = DI {
        bindEagerSingleton<ConfigProvider> { ConfigProvider.Default }
        bindEagerSingleton<Config> { instance<ConfigProvider>() + ConfigKtor(application.environment.config) }

        bindSet<KtorApplication>()
        bindSet<KtorRoute>()

        builder.invoke(this)

        inSet<KtorApplication> { singleton { RoutingModule(routes = instance()) } }
        inSet<KtorApplication> { singleton { LoggingModule() } }
        inSet<KtorApplication> { singleton { ContentNegotiationModule() } }
        inSet<KtorApplication> { singleton { CorsModule() } }
        inSet<KtorApplication> { singleton { HealthCheckModule() } }
        inSet<KtorApplication> { singleton { StatusPageModule() } }
    }

    // TODO:  IMicroserviceAction переделать на фазы ktor app

    application.attributes.put(diAttributeKey, di)

    val plugins by di.instance<Set<KtorApplication>>()

    plugins.forEach {
        it.install(application)
    }
}

suspend inline fun <reified T : Any> ApplicationCall.respondResponse(response: Response<T>) { // TODO: rename
    when (response) {
        is Response.Success -> {
            if (response.value != NoContent) {
                respond(HttpStatusCode.fromValue(response.status.value), response.value)
            } else {
                respond(HttpStatusCode.fromValue(response.status.value))
            }
        }

        is Response.Error -> {
            val errorsDto = ErrorsDto(
                errors = response.errors.map {
                    ErrorDto(
                        code = it.code,
                        description = it.description
                    )
                }
            )
            respond(HttpStatusCode.fromValue(response.status.value), errorsDto)
        }
    }
}

interface IMicroserviceAction {
    fun execute(di: DI)
}

fun DI.Builder.withApp(body: Application.() -> Unit) {
    inSet<KtorApplication> {
        singleton {
            KtorApplication { application -> body(application) }
        }
    }
}

fun DI.Builder.doBeforeStart(body: DI.() -> Unit) {
    inSet<IMicroserviceAction> {
        singleton {
            object : IMicroserviceAction {
                override fun execute(di: DI) {
                    body.invoke(di)
                }
            }
        }
    }
}
