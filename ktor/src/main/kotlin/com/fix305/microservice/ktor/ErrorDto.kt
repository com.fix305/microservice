package com.fix305.microservice.ktor

import kotlinx.serialization.Serializable

@Serializable
data class ErrorDto(
    /** Код ошибки */
    val code: String = "",
    /** Описание ошибки */
    val description: String = "",
)