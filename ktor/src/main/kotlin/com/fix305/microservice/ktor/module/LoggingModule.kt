package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.callid.*
import io.ktor.server.plugins.callloging.*
import io.ktor.server.request.*
import org.slf4j.event.Level

class LoggingModule : KtorApplication {
    override fun install(application: Application) {
        with(application) {
            install(CallLogging) {
                callIdMdc("call-id")

                level = Level.INFO

                filter { call -> call.request.path().startsWith("/") }

                disableDefaultColors()
            }
            install(CallId) {
                header(HttpHeaders.XRequestId)
                verify { callId: String ->
                    callId.isNotEmpty()
                }
            }
        }
    }
}
