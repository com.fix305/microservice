package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import com.fix305.microservice.ktor.KtorRoute
import io.ktor.server.application.*
import io.ktor.server.routing.*

/** Модуль установки роутинга в Ktor */
class RoutingModule(private val routes: Set<KtorRoute>) : KtorApplication {
    override fun install(application: Application) {
        application.routing {
            routes.forEach {
                it.install(this)
            }
        }
    }
}
