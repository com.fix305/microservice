package com.fix305.microservice.ktor

import com.fix305.common.config.Config
import com.fix305.common.config.ConfigValue
import io.ktor.server.config.*

/**
 * Конфигурация на основе данных из ktor.
 * На вход принимает [ApplicationConfig]
 */
class ConfigKtor(
    private val applicationConfig: ApplicationConfig
) : Config {
    override fun getOrThrow(name: String): ConfigValue {
        return applicationConfig.propertyOrNull(name)?.toValue()
            ?: error("Параметр конфигурации `$name` не задан")
    }

    override fun getOrNull(name: String): ConfigValue? {
        return applicationConfig.propertyOrNull(name)?.toValue()
    }

    override fun getConfig(name: String): Config {
        return ConfigKtor(applicationConfig.config(path = name))
    }

    override fun getConfigList(name: String): List<Config> {
        return applicationConfig.configList(path = name).map {
            ConfigKtor(it)
        }
    }

    private fun ApplicationConfigValue.toValue(): Value {
        return Value(this)
    }

    data class Value(
        private val value: ApplicationConfigValue
    ) : ConfigValue {
        override fun getString(): String = value.getString()
        override fun getList(): List<String> = value.getList()
    }
}
