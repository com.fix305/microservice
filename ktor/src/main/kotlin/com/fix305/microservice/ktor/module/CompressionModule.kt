package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.server.application.*
import io.ktor.server.plugins.compression.*

class CompressionModule : KtorApplication {
    override fun install(application: Application) {
        with(application) {
            install(Compression) {
                gzip {
                    priority = 1.0
                }
                deflate {
                    priority = 10.0
                    minimumSize(1024) // condition
                }
            }
        }
    }
}

