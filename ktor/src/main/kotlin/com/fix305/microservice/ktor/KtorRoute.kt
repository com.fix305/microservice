package com.fix305.microservice.ktor

import io.ktor.server.routing.*

/** Интерфейс добавления роутинга в ktor */
interface KtorRoute {
    /** Установить роутинг */
    fun install(routing: Routing)
}
