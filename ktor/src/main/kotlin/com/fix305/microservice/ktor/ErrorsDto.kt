package com.fix305.microservice.ktor

import kotlinx.serialization.Serializable

@Serializable
data class ErrorsDto(
    val errors: List<ErrorDto> = emptyList(),
)