package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

/** Модуль healths check  */
class HealthCheckModule : KtorApplication {
    override fun install(application: Application) {
        with(application) {
            routing {
                get("healthz") {
                    call.respond(true)
                }
            }
        }
    }
}
