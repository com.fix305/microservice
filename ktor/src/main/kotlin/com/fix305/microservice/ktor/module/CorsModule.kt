package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*


/** Модуль обработки CORS политики */
// TODO: нужны настройки через конфиг
class CorsModule : KtorApplication {
    override fun install(application: Application) {
        with(application) {
            install(CORS) {
                anyHost()
                allowHeaders { true }
                allowNonSimpleContentTypes = true
            }
        }
    }
}
