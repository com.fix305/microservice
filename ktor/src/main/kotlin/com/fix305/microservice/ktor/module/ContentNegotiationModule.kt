package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.serialization.json.Json

/** Модуль обработки JSON при запросах */
class ContentNegotiationModule : KtorApplication {
    override fun install(application: Application) {
        with(application) {
            val jsonBuilder = Json {
                prettyPrint = false
                isLenient = true
                ignoreUnknownKeys = true
            }

            install(ContentNegotiation) { json(jsonBuilder) }
        }
    }
}
