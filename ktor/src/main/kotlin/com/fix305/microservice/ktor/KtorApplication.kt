package com.fix305.microservice.ktor

import io.ktor.server.application.*

/** Интерфейс для установки модулей Ktor */
fun interface KtorApplication {
    /** Установка модуля */
    fun install(application: Application)
}
