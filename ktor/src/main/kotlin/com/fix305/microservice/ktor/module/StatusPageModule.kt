package com.fix305.microservice.ktor.module

import com.fix305.microservice.ktor.KtorApplication
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*

/**
 * Install handlers of exception for ktor response
 */
class StatusPageModule : KtorApplication {
    override fun install(application: Application) {
        application.install(StatusPages) {
            exception<AuthenticationException> { call, cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { call, cause ->
                call.respond(HttpStatusCode.Forbidden)
            }
            exception<InternalServerErrorException> { call, exception ->
                call.application.environment.log.error(exception.message, exception)

                call.respond(HttpStatusCode.InternalServerError, exception.message)
            }

            exception<Exception> { call, exception ->
                call.application.environment.log.error(exception.message ?: "Something went wrong", exception)

                call.respond(HttpStatusCode.InternalServerError, exception.message ?: "Something went wrong")
            }
        }
    }

    class InternalServerErrorException(
        override val message: String,
        override val cause: Throwable? = null
    ): RuntimeException()

    class AuthenticationException : RuntimeException()
    class AuthorizationException : RuntimeException()
}
