package com.fix305.microservice.ktor.test_fixtures

import com.fix305.common.config.Config
import com.fix305.common.config.ConfigProvider
import com.fix305.microservice.ktor.ConfigKtor
import com.fix305.microservice.ktor.KtorApplication
import com.fix305.microservice.ktor.KtorRoute
import com.fix305.microservice.ktor.module.*
import com.typesafe.config.ConfigFactory
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.config.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.*
import org.kodein.di.*

internal class TestKtorServerImpl(
    private val host: String = "127.0.0.1",
    private val port: Int = 9305,
    private val configBasename: String? = null,
    builder: DI.Builder.() -> Unit,
) : TestKtorServer {
    private var _app: Application? = null
    override val app: Application get() = _app ?: error("App didn't run")
    override val baseUrl: Url = Url("http://$host:$port")

    private val di = DI {
        bindEagerSingleton<ConfigProvider> { ConfigProvider.Default }
        bindEagerSingleton<Config> { instance<ConfigProvider>() }

        bindSet<KtorApplication>()
        bindSet<KtorRoute>()

        builder.invoke(this)

        inSet<KtorApplication> { singleton { ContentNegotiationModule() } }
        inSet<KtorApplication> { singleton { CorsModule() } }
        inSet<KtorApplication> { singleton { LoggingModule() } }
        inSet<KtorApplication> { singleton { HealthCheckModule() } }

        inSet<KtorApplication> { singleton { RoutingModule(routes = instance()) } }

        bindSingleton<ApplicationEngine> {
            val configProvider = instance<ConfigProvider>()

            embeddedServer(Netty, applicationEngineEnvironment {
                config = if (configBasename != null) {
                    HoconApplicationConfig(ConfigFactory.load(configBasename))
                } else {
                    HoconApplicationConfig(ConfigFactory.load())
                }
                connector {
                    host = this@TestKtorServerImpl.host
                    port = this@TestKtorServerImpl.port
                }
                module {
                    _app = this
                    this.attributes.put(diAttributeKey, di)

                    configProvider.add(ConfigKtor(app.environment.config))

                    val plugins by di.instance<Set<KtorApplication>>()

                    plugins.forEach {
                        it.install(this)
                    }
                }
            })
        }
    }

    private val engine by di.instance<ApplicationEngine>()

    override suspend fun start() {
        engine.start(false)
    }

    override fun close() {
        engine.stop(10, 10)
    }

    companion object {
        private val diAttributeKey = AttributeKey<DI>("di")
    }
}
