package com.fix305.microservice.ktor.test_fixtures

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.utils.io.core.*

interface TestKtorServer : Closeable {
    val app: Application
    val baseUrl: Url
    suspend fun start()
}
