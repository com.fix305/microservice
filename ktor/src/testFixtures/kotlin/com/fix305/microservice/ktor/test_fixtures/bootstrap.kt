package com.fix305.microservice.ktor.test_fixtures

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.server.application.*
import kotlinx.coroutines.coroutineScope
import org.kodein.di.DI

/**
 * Make request to web-server
 * Example:
 * ```
 * val response = httpRequest("http://$host:$port") {
 *     method = HttpMethod.Post
 *     contentType(ContentType.Application.Json)
 *     url("scheduler/scheduleTask")
 *
 *     setBody(json)
 * }
 *
 * response.status shouldBe HttpStatusCode.OK
 * ```
 */
suspend fun httpRequest(
    baseUrl: String = "http://127.0.0.1:9305",
    body: HttpRequestBuilder.() -> Unit
): HttpResponse {
    val client = HttpClient(CIO) {
        defaultRequest {
            url(baseUrl)
        }
    }

    return client.request { body(this) }
}

/**
 * Make and run simple web-server.
 * Example:
 * ```
 * val httpclient = HttpClient(CIO)
 *
 * testMicroservice("127.0.0.1", 8080) {
 *      routing {
 *          get("some-url") {
 *              call.respond("Hello, world!")
 *          }
 *      }
 * }.use {
 *      val result: String = httpclient.get("http://127.0.0.1:8080/some-url")
 *
 *      result shouldBe "Hello, world!"
 * }
 * ```
 */
suspend fun testMicroservice(
    host: String = "127.0.0.1",
    port: Int = 9305,
    body: Application.() -> Unit,
): TestKtorServer = coroutineScope {
    SimpleAppServer(host, port, body).also {
        it.start()
    }
}

/**
 * Make and run simple web-server with DI.
 * Example:
 * ```
 * val httpclient = HttpClient(CIO)
 *
 * testDiMicroservice("127.0.0.1", 8080) {
 *      import(routes)
 * }.use {
 *      val result: String = httpclient.get("http://127.0.0.1:8080/some-url")
 *
 *      result shouldBe "Hello, world!"
 * }
 * ```
 */
suspend fun testDiMicroservice(
    host: String = "127.0.0.1",
    port: Int = 9305,
    configBasename: String? = null,
    builder: DI.Builder.() -> Unit,
): TestKtorServer = coroutineScope {
    TestKtorServerImpl(
        host = host,
        port = port,
        configBasename = configBasename,
        builder = builder
    ).also { it.start() }
}
