package com.fix305.microservice.ktor.test_fixtures

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*

internal class SimpleAppServer(
    private val host: String = "127.0.0.1",
    private val port: Int = 9305,
    body: Application.() -> Unit,
) : TestKtorServer {
    private var _app: Application? = null
    override val app: Application get() = _app ?: error("App didn't run")
    override val baseUrl: Url = Url("http://$host:$port")

    private val engine by lazy {
        embeddedServer(Netty, port = port, host = host) {
            _app = this
            body(this)
        }
    }

    override suspend fun start() {
        engine.start(false)
    }

    override fun close() {
        engine.stop(10, 10)
    }
}
