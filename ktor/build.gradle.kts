plugins {
    id("org.jetbrains.kotlin.jvm") version "1.9.0"
    kotlin("plugin.serialization") version "1.9.0"

    id("maven-publish")

    `java-library`
    `java-test-fixtures`
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/50393262/packages/maven")
}

val ktorVersion: String by project
val kotestVersion: String by project
val logbackVersion: String by project
val logbackEncoderVersion: String by project
val kodeinVersion: String by project
val fix305CommonVersion: String by project

dependencies {
    implementation("com.fix305.common:config:$fix305CommonVersion")
    api("com.fix305.common:response:$fix305CommonVersion")

    // KODEIN DI
    api("org.kodein.di:kodein-di:$kodeinVersion")

    // KTOR
    api("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-auth-jvm:$ktorVersion")
    api("io.ktor:ktor-server-auth-jwt-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-compression-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-cors-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-call-logging-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-call-id-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-metrics-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
    api("io.ktor:ktor-server-netty-jvm:$ktorVersion")

    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("net.logstash.logback:logstash-logback-encoder:$logbackEncoderVersion")

    testImplementation("io.ktor:ktor-server-tests:$ktorVersion")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotestVersion")

    // TEST
    testFixturesImplementation("com.fix305.common:config:$fix305CommonVersion")
    testFixturesImplementation("io.ktor:ktor-client-core:$ktorVersion")
    testFixturesImplementation("io.ktor:ktor-client-cio:$ktorVersion")

    testFixturesImplementation("io.ktor:ktor-server-core:$ktorVersion")
    testFixturesImplementation("io.ktor:ktor-server-netty:$ktorVersion")
}

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/50390469/packages/maven")
            name = "GitLab"
            version = findProperty("version") ?: "undefined"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
